import Vue from 'vue'
import store from "@/store";
import Home from '../views/Home'
import VueRouter from 'vue-router'
import NotFound from '../views/NotFound'
import Login from '../views/Login'
import Register from '../views/Register'
import Profile from '../views/Profile'
import vehiculo from '../views/Vehiculo'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/nuevo/vehiculo',
    name: 'vehiculo',
    component: vehiculo
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0)
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isAuthenticated = store.state.user.isAuthenticated
  if (requiresAuth && !isAuthenticated) {
    next('/')
  } else {
    next()
  }
})
  

export default router
