import Vue from 'vue'

import App from './App.vue'
import store from './store'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Vuelidate from "vuelidate";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import axios from 'axios'
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL
console.log(process.env.VUE_APP_BASE_URL)
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.devtools = false
Vue.config.errorHandler = (err) => {
  if (process.env.NODE_ENV !== 'production') {
    // Show any error but this one
    if (err.message !== "Cannot read property 'badInput' of undefined") {
        console.log(err);
    }
  }
};

function getAccessToken() {
  return localStorage.getItem('usertoken');
}
axios.interceptors.request.use(request => {
  let token=getAccessToken()
  if(token !==null){
      request.headers['Authorization'] = "Bearer " + getAccessToken();
  }
  return request;
});
const reqInterceptor = axios.interceptors.request.use(config => {
  return config
})
const resInterceptor = axios.interceptors.response.use(res => {
  return res
})
axios.interceptors.request.eject(reqInterceptor)
axios.interceptors.response.eject(resInterceptor)

Vue.use(Vuelidate);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created () {

  }  
}).$mount('#app')
