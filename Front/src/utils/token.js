import store from '@/store/'

class TokenService {
  
    getToken() {
      return localStorage.getItem("userToken");
    }
  
    setToken(token) {
      store.dispatch('setAccessToken', token)
      localStorage.setItem("userToken", token);
    }


    getTokenRefresh() {
      return localStorage.getItem("userTokenRefresh");
    }
  
    setTokenRefresh(token) {
      store.dispatch('setRefreshToken', token)
      localStorage.setItem("userTokenRefresh", token);
    }  
    LogOut(){
      store.dispatch('LogOut',)
    }  
  }
  
  export default new TokenService();