import axios from 'axios'
import store from '@/store'
import { i18n } from '../main'
import { Toast } from 'vant'
import TokenService from "./token";
import createAuthRefreshInterceptor from "axios-auth-refresh";


const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_SCT_SVC_BACK_BASE_URL, // url = base api url + request url
    //withCredentials: true, // send cookies when cross-domain requests
    timeout: 15000 // request timeout
})

let toastLoading;

axiosInstance.interceptors.request.use(
    (config) => {
      if(!store.getters['connected']){
        Toast(i18n.t("lang.etiquetas.sinConexionAInternet"))
        return
      }
      if(!config.closeLoading){
        toastLoading = Toast.loading({
          message: i18n.t("lang.etiquetas.cargando")
        });
      }      
      const token =  TokenService.getToken();
      const headers = { ...config.headers };
      if (token) {
         headers.Authorization = `Bearer ${token}`;
      }
      return { ...config, headers };
    },(error) => {
        return Promise.reject(error);
      }
);

axiosInstance.interceptors.response.use(res => {
    toastLoading.clear();
    const code = res.data.code || 200;
    const msg = errorCode[code] || res.data.msg || errorCode['default']
    if (code === 401) {
      Toast(msg);
    } else if (code === 500) {
      Toast(msg);        
      return Promise.reject(new Error('msg'))
    } else if (code !== 200 && code !== 201) {
      Toast(msg);        
      return Promise.reject('error')
    } else {
      return res.data
    }
  },
  error => {
    console.log(error.response)
    toastLoading?.clear();       
    let { message } = error;
    if (message == "Network Error") {
      message = "Ocurrió un error en la conexión";
    }
    else if (message.includes("timeout")) {
      message = "Se agotó el tiempo de espera de la solicitud";
    }
    else if (message.includes("Request failed with status code")) {
      message = 'Solicitud fallida: ' + message.substr(message.length - 3);
    }
    console.log(message)
    return Promise.reject(error)
  }
)

const errorCode = {
    '401': 'Error de autenticación, no se puede acceder a los recursos del sistema',
    '403': 'La operación actual no tiene permiso',
    '404': 'El recurso de acceso no existe',
    'default': 'Error de sistema desconocido, por favor envíe sus comentarios al administrador'
}


const refreshAuthLogic = (failedRequest) => axios.post(
  process.env.VUE_APP_SCT_SVC_BACK_BASE_URL + '/token/refresh/',
  {
    refresh: TokenService.getTokenRefresh()
  }
).then(tokenRefreshResponse => {
  const {data} = tokenRefreshResponse
  TokenService.setToken(data.access)
  failedRequest.response.config.headers["Authorization"] = 'Bearer ' + tokenRefreshResponse.access
    return Promise.resolve();
})
.catch(async () => {
  TokenService.LogOut() 
  if(window.location.pathname !== '/')
    setTimeout(relocate,3000);       
})
function relocate(){
  const toast = Toast({
    message:  i18n.t("lang.etiquetas.tuSesionExpiro"),
    duration: 0, // continuous display toast
    forbidClick: true,
  });
  let second = 5;
  const timer = setInterval(() => {
    second--;
    if (second) {
      toast.message =i18n.t("lang.etiquetas.tuSesionExpiro");
    } else {
      clearInterval(timer);
      Toast.clear();
      if(window.location.pathname !== '/')window.location.replace('/')
    }
  }, 1000);  
}

const statusCodes = {
    statusCodes: [ 401, 403 ] // default: [ 401 ]
  }
createAuthRefreshInterceptor(axiosInstance, refreshAuthLogic, statusCodes);


export default axiosInstance