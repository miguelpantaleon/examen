import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        cuestionarios:{
            vuelo:null,
            owner:[],
            guest:[],
        },
        user:{
            id:null,
            preloadData:false,
            token: null,
            refreshToken: null,
            isAuthenticated: false,
            vuelos:[],
            data:{
                nombre:null,
               
            }
        },
        
    },
    mutations: {
        
        set_connected(state, payload) {
            state.connected = payload
        },
        setAuthenticated: function(state, isAuthenticated) {
            state.user.isAuthenticated = isAuthenticated
        },  
        setAccessToken: function(state, token) {
            state.user.token = token;
          },   
        setRefreshToken: function(state, refreshToken) {
            state.user.refreshToken = refreshToken;
        },  
        LogOut(state){
            state.user.id = null
            state.user.token  = null
            state.user.refreshToken = null
            state.user.isAuthenticated = false
            state.user.data = {}

        },
        setUserDta: function(state, data) {
            state.user.id = data.id
            state.user.data = data
        },
       
        updateUsrProperty: function(state, { prop, val }){
            state.user[prop] = val
        },
        

    },
    actions: {       
        setConnected ({ commit }, payload) {
            commit('set_connected', payload)
            },   
        setAccessToken ({ commit }, payload) {
            commit('setAccessToken', payload)
        },
        setRefreshToken ({ commit }, payload) {
            commit('setRefreshToken', payload)
        },     
        LogOut ({ commit },) {
            commit('LogOut',)
        },                                
    },     
    getters: {
        isAuthenticated: state => !!state.user.isAuthenticated,    
        connected: ( state ) => state.connected,
        getUsrData: ( state ) => state.user.data
    },   
   
})
  