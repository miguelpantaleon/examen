import request from '@/utils/request'

export function registro(data){
    return request({
        url: 'registro/',
        method:'post',
        data
    })
}

export function login(data){
    return request({
        url:'token/',
        method:'post',
        data
    })
}

export function getUser(){
    return request({
        url:'user/',
        method:'get'
    })
}

export function getDataPasajero(){
    return request({
        url: 'datos/pasajero',
        method:'post'
    })
}

