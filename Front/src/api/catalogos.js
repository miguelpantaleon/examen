import request from '@/utils/request'

export function getAereolinias() {
    return request({
      url: 'aereolinea/',
      method: 'get'
    })
}

export function getCiudades(){
    return request ({
        url: 'ciudad/',
        method: 'get'
    })
}

export function getPaises() {
    return request({
        url: 'pais/',
        method: 'get'
    })
}

export function getSintomas() {
    return request({
        url:'sintoma/',
        method: 'get'
    })
}