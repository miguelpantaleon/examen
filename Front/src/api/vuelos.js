import request from '@/utils/request'

export function getVuelos(data){
    return request({
        url:'listado/vuelos',
        method:'post', data
    })
}

export function saveCuestionario(data){
    return request({
        url:'guardar/encuesta',
        method:'post',
        data
    })
}

export function saveAcompaniante(data){
    return request({
        url:'guardar/acompaniante',
        method:'post',
        data
    })
}