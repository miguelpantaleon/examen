<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehiculo extends Model
{
    use HasFactory;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function color()
    {
        return $this->belongsTo('App\color', 'id_color', 'id_color');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipoVehiculo()
    {
        return $this->belongsTo('App\tipo_vehiculo', 'id_tipo', 'id_tipo');
    }
}
