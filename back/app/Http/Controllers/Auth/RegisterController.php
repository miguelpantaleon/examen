<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'apellido_paterno' => ['required', 'string'],
            'apellido_materno' => ['required', 'string'],
            'provincia'=> ['required'],
            'ciudad' => ['required'],
            'telefono' => ['required'],
            'colonia' => ['required'],
            'calle' => ['required'],
            //'numero_exterior' => ['required'],
            //'numero_interior' => ['integer'],
            'codigo_postal' => ['required'],
            'fecha_nacimiento' => ['required'],
            'g-recaptcha-response' => 'recaptcha',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'apellido_paterno' => $data['apellido_paterno'],
            'apellido_materno' => $data['apellido_materno'],
            'provincia' => $data['provincia'],
            'ciudad' => $data['ciudad'],
            'telefono' => $data['telefono'],
            'colonia' => $data['colonia'],
            'calle' => $data['calle'],
            'codigo_postal' => $data['codigo_postal'],
            'numero_exterior' => NULL,
            'numero_interior' => NULL,
            'fecha_nacimiento' => $data['fecha_nacimiento'],
            'dir_ip' => request()->ip()

        ]);
    }
}
